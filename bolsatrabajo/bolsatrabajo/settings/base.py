"""
Django settings for bolsatrabajo project.

Generated by 'django-admin startproject' using Django 1.11.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

from pathlib import Path
from tools.environment.get_env import get_env_variable
import os
import sys

SITE_ID=1


# Template values
SITE_NAME = "Bolsa de Trabajo Catherine Flon"
META_DESCRIPTION = "Sitio para gestionar trabajos dignos para inmigrantes Haitianos"




# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
SETTINGS_PATH = Path(__file__)
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = str(SETTINGS_PATH.resolve().parent.parent.parent)



# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_env_variable('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!


ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    # https://django-admin-tools.readthedocs.io/en/latest/index.html
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    #BASE:
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    # admin docs
    "django.contrib.admindocs",
    # CKEDITOR
    'ckeditor',
    # uploader
    'ckeditor_uploader',
   # Django bleach: HTML sanitized
    'django_bleach',
    #https://github.com/kaleidos/django-mathjax
    'django_mathjax',
    #Django bower
    'djangobower',
    # Django extensions
    # http://django-extensions.readthedocs.io/en/latest/installation_instructions.html
    'django_extensions',
    # Django Compressor
    # https://django-compressor.readthedocs.io/en/latest/quickstart/
    "compressor",
    #Imagekit
    #https://github.com/matthewwithanm/django-imagekit
    'imagekit',    

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',

]

ROOT_URLCONF = 'bolsatrabajo.urls'

FILES_DIR = os.path.join(BASE_DIR, 'files')

PROJECTS_DIR = os.path.join(BASE_DIR, 'apps')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(FILES_DIR, "templates")],
        #'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'context.settings.global_values'
            ],
            'loaders':[
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                ]),
                'admin_tools.template_loaders.Loader'
            ]
        },
    },
]

WSGI_APPLICATION = 'bolsatrabajo.wsgi.application'




# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'es-CL'

TIME_ZONE = 'UTC-3'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'


STATICFILES_DIRS = [
   #os.path.join(FILES_DIR,'static'),
   #os.path.join(PROJECTS_DIR, "localizacion/static/"),
]


STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
]

STATIC_ROOT = os.path.normpath(os.path.join(FILES_DIR, "static"))

MEDIA_ROOT = os.path.normpath(os.path.join(FILES_DIR, "media"))

BOWER_COMPONENTS_ROOT = os.path.normpath(os.path.join(FILES_DIR, "components"))

BOWER_INSTALLED_APPS = (
    'underscore',
)

# Configuracion CKEDITOR

CKEDITOR_UPLOAD_SLUGIFY_FILENAME = True
CKEDITOR_RESTRICT_BY_USER = True
CKEDITOR_BROWSE_SHOW_DIRS = True

# Upload path
CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_IMAGE_BACKEND = 'pillow'

CKEDITOR_CONFIGS = {
    'default': {
        'removePlugins': 'stylesheetparser', 'bbcode'
        'toolbar': None,
        'height': 500,
        'width': 900,
        'tabSpaces': 4,
        'allowedContent': True,
        'extraPlugins': ','.join(
            [
                # you extra plugins here
                'ajax',
                'div',
                'autolink',
                'autoembed',
                'embedsemantic',
                'autogrow',
                # 'devtools',code,
                'image',
                'widget',
                'lineutils',
                'clipboard',
                'dialog',
                'dialogui',
                'codesnippetgeshi',
                'elementspath',
                'table',
                'tableresize',
                'tabletools',
                'templates',
                'uicolor',
                'uploadimage',
                'uploadwidget',
                'mathjax',
                'pagebreak',
                'placeholder'
            ]),
    },
}


# Bleach filter html
BLEACH_DEFAULT_WIDGET = 'ckeditor.widgets.CKEditorWidget'
# Which HTML tags are allowed
BLEACH_VALID_TAGS = ['p', 'b', 'i', 'strike', 'ul', 'li', 'ol', 'br',
                     'span', 'blockquote', 'hr', 'a', 'img']
BLEACH_VALID_ATTRS = {
    'span': ['style', ],
    'p': ['align', ],
    'a': ['href', 'rel'],
    'img': ['src', 'alt', 'style'],
}
BLEACH_VALID_STYLES = ['color', 'cursor', 'float', 'margin']
BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'name']
BLEACH_STRIP_TAGS = True

# DATE FORMAT

DATE_INPUT_FORMATS = ['%d/%m/%Y', '%d %m %Y', '%d-%m-%Y']


#Gestión de passwords
# Argon, campeon 2015

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

AUTH_PASSWORD_VALIDATORS=[
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

