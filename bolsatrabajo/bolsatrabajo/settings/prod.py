import os
from .base import *

DEBUG = False

# GOOGLE ANALYTICS PARAMETERS

GOOGLE_ANALYTICS=''
GOOGLE_API_KEY=''

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DBNAME=get_env_variable('DBNAME')
DBUSER=get_env_variable('DBUSER')
DBPASS=get_env_variable('DBPASS')

DATABASES = {
    'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': DBNAME,
         'USER': DBUSER,
         'PASSWORD':DBPASS,
         'HOST': '',
         'PORT': '',
    },
}
