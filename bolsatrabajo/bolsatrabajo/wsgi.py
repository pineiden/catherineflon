"""
WSGI config for bolsatrabajo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from tools.environment.get_env import get_env_variable


DJANGO_STATUS=get_env_variable("DJANGO_STATUS")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", DJANGO_STATUS)

application = get_wsgi_application()
