from django.conf import settings

def global_values(request):
    my_dict = {
        'SITE_URL': settings.SITE_URL,
        'SITE_NAME': settings.SITE_NAME,
        'GOOGLE_ANALYTICS': settings.GOOGLE_ANALYTICS,
		'GOOGLE_API_KEY': settings.GOOGLE_API_KEY
    }

    return my_dict
